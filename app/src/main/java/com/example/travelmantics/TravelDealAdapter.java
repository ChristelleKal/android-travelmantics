package com.example.travelmantics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class TravelDealAdapter  extends RecyclerView.Adapter<TravelDealAdapter.DealViewHolder> {

    ArrayList<TravelDeal> deals;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ChildEventListener childEventListener;
    private  ImageView imageDeal;

    public TravelDealAdapter(Activity activity)
    {
        // set up firebase reference
        FireBaseUtils.openFbreference("traveldeals", (ListActivity) activity);

        // create n instance of the firebaseDB
        firebaseDatabase = FireBaseUtils.firebaseDatabase;

        // Create a reference to the DB
        databaseReference = FireBaseUtils.databaseReference;

        // Crate a list of travel deals
        deals = FireBaseUtils.deals;

        // create  a listener
        childEventListener  = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                // get the enw travel deal object inserted
                TravelDeal td = dataSnapshot.getValue(TravelDeal.class);
                // set the travel deal id
                td.setId(dataSnapshot.getKey());
                // add the travel deal onjct to the list
                deals.add(td);
                //  notify the observer that the list has changed so that the ui will b updated
                notifyItemInserted(deals.size() - 1);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        databaseReference.addChildEventListener(childEventListener);


    }
    @NonNull
    @Override
    public DealViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.rv_row,parent, false);

        return new DealViewHolder( itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DealViewHolder holder, int position) {
        TravelDeal currentDeal = deals.get(position);
        holder.bind(currentDeal);

    }

    @Override
    public int getItemCount() {
        return  deals.size();
    }

    public class DealViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvTitle ;
        TextView tvDescrption;
        TextView tvPrice;


        public DealViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescrption = itemView.findViewById(R.id.tvDescription);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            imageDeal = itemView.findViewById(R.id.imageDeal);

            // set the click listener event
            itemView.setOnClickListener(this);
        }

        public void bind(TravelDeal deal)
        {
            tvTitle.setText(deal.getTitle());
            tvDescrption.setText(deal.getDescription());
            tvPrice.setText(deal.getPrice());
            showImage(deal.getImageurl());

        }

        private void showImage(String url){
            if(url != null && url.isEmpty() == false){
                int width = Resources.getSystem().getDisplayMetrics().widthPixels;
                Picasso.get().load(url).resize(140, 140).centerCrop().into(imageDeal);
            }
        }

        @Override
        public void onClick(View view) {
            // get the position of the item clicked
            int position = getAdapterPosition();
            // get the travel deal at the selected index
            TravelDeal selectedDeal =  deals.get(position);

            //Create an intent to display the selected deal
            Intent dealIntent = new Intent(view.getContext(), DealActivity.class);
            dealIntent.putExtra("deal", selectedDeal);

            // start activity from the context of the current view
            view.getContext().startActivity(dealIntent);


        }
    }
}
