package com.example.travelmantics;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class ListActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_activity_menu, menu);

        MenuItem insertMenu = menu.findItem(R.id.insert_menu);
        if(FireBaseUtils.isAdmin == true)
        {
            insertMenu.setVisible(true);
        }
        else{
            insertMenu.setVisible(false);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.insert_menu:
                Intent insertIntent = new Intent(this, DealActivity.class);
                startActivity(insertIntent);
                return true;
            case R.id.logout_menu:
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                               FireBaseUtils.attachListener();
                            }
                        });
                FireBaseUtils.detachListener();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        FireBaseUtils.detachListener();
    }

    @Override
    protected void onResume() {
        super.onResume();



        FireBaseUtils.openFbreference("traveldeal", this);
        // Create a recylcerview
        RecyclerView travelDealsRecycleView = findViewById(R.id.rvDeals);
        // Create an adapter
        TravelDealAdapter  travelDealAdapter = new TravelDealAdapter(this);

        // set adapter of the recyler view
        travelDealsRecycleView.setAdapter( travelDealAdapter);

        // Create a linear layout manager object
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);

        // Set layout of the recycler view
        travelDealsRecycleView.setLayoutManager(linearLayoutManager);

        FireBaseUtils.attachListener();

    }

    public void showMenu()
    {
        invalidateOptionsMenu();
    }
}
