package com.example.travelmantics;

import android.app.Activity;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FireBaseUtils {

    public static  FirebaseDatabase firebaseDatabase;
    public static DatabaseReference databaseReference;

    public static FirebaseAuth firebaseAuth;
    public static FirebaseAuth.AuthStateListener authStateListener;

    public static FirebaseStorage firebaseStorage;
    public static StorageReference storageReference;

    private static FireBaseUtils fireBaseUtils;
    public static ArrayList<TravelDeal> deals;
    private  static ListActivity callerActivity;
    private static final int RC_SIGN_IN = 123;

    public static boolean isAdmin;

    // Private constructor
    private  FireBaseUtils(){}

    public static void openFbreference(String ref, final ListActivity staterActivity)
    {
        if (fireBaseUtils == null)
        {
            fireBaseUtils = new FireBaseUtils();
            firebaseDatabase = FirebaseDatabase.getInstance();
            firebaseAuth = FirebaseAuth.getInstance();
            callerActivity = staterActivity;
            authStateListener =  new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    if (firebaseAuth.getCurrentUser() == null) {
                        FireBaseUtils.signIn();
                    }
                    else
                    {
                        String userId = firebaseAuth.getUid();
                        checkAdmin(userId);
                    }
                    Toast.makeText(callerActivity.getBaseContext(), "Welcome back!", Toast.LENGTH_LONG).show();


                }
            };

        }
        deals = new ArrayList<>();
        databaseReference = firebaseDatabase.getReference().child(ref);
        connectStorage();

    }

    public static void checkAdmin(String uid) {
        FireBaseUtils.isAdmin = false;
        DatabaseReference ref = firebaseDatabase.getReference().child("Administrators").child(uid);

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                fireBaseUtils.isAdmin = true;
                callerActivity.showMenu();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        ref.addChildEventListener(listener);
    }

    private static void signIn()
    {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build()
        );

        // Create and launch sign-in intent
        callerActivity.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }
    /**
     * Method to check if the user is not authenticated to attchhe an authentication mechanism
     */
    public static void attachListener()
    {
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    /***
     * Mehtod to remove the authentication listener
     */
    public static void detachListener(){
        firebaseAuth.removeAuthStateListener(authStateListener);
    }


    /**
     * Method to connect to firebase storage
     */

    public static  void connectStorage(){
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference().child("dealsPicture");

    }


}
