package com.example.travelmantics;

import java.io.Serializable;

public class TravelDeal implements Serializable {

    private String id;
    private String title;
    private String price;
    private String description;
    private String imageurl;


    private String imagName;

    public TravelDeal( String title, String price, String description, String imageurl) {
        //this.setId(id);
        this.setTitle(title);
        this.setPrice(price);
        this.setDescription(description);
        this.setImageurl(imageurl);
    }

    public TravelDeal(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }


    public String getImagName() {
        return imagName;
    }

    public void setImagName(String imagName) {
        this.imagName = imagName;
    }

    @Override
    public String toString() {

        return"Title: " +  this.getTitle() + "\n" + "Price: "  +  this.getPrice() + "\n" + "Description: " + this.getDescription() + "\n";
    }
}
