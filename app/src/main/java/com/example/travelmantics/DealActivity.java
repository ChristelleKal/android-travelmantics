package com.example.travelmantics;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class DealActivity extends AppCompatActivity {

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    private ImageView imageDeal;
    private EditText txtTitle ;
    private EditText txtPrice ;
    private EditText txtDescription ;
    private TravelDeal deal;
    private static final int PICTURE_HANDLE = 42;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal);

        // create n instance of the firebaseDB
        firebaseDatabase = FireBaseUtils.firebaseDatabase;

        // Create a reference to the DB
        databaseReference = FireBaseUtils.databaseReference;

        // Create instance to edit texts
        txtTitle = findViewById(R.id.txtTitle);
        txtPrice = findViewById(R.id.txtPrice);
        txtDescription = findViewById(R.id.txtDescription);
        imageDeal = findViewById(R.id.imageView);

        // get intent
        Intent intent = getIntent();
        TravelDeal  travelDeal = (TravelDeal) intent.getSerializableExtra("deal");

        // create a empty travel deal when the deal from intent is empty
        if(travelDeal == null)
        {
            travelDeal = new TravelDeal();
        }
        this.deal = travelDeal;

        txtTitle.setText(deal.getTitle());
        txtDescription.setText(deal.getDescription());
        txtPrice.setText(deal.getPrice());
        showImage(deal.getImageurl());

        Button btnUpload = findViewById(R.id.btnImage);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/jpeg");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                startActivityForResult(intent.createChooser(intent, "Insert picture"),PICTURE_HANDLE );

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == PICTURE_HANDLE && resultCode == RESULT_OK){
            Uri imageUri = data.getData();
            final StorageReference storageRef = FireBaseUtils.storageReference.child(imageUri.getLastPathSegment());
            UploadTask uploadTask = storageRef.putFile(imageUri);

            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return storageRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        deal.setImageurl( downloadUri.toString());
                        String pictureName = task.getResult().getPath();
                        deal.setImagName(pictureName);


                        showImage(downloadUri.toString());
                    } else {

                    }
                }
            });


        }
    }

    /**
     *  method to handle menu item selected
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.save_menu:
                saveDeal();
                Toast.makeText(this, R.string.saved_deal_text, Toast.LENGTH_SHORT).show();
                clean();
                backTolist();
                return true;
            case R.id.delete_menu:
                deleteDeal();
                Toast.makeText(this,"Deal Deleted ", Toast.LENGTH_SHORT).show();
                backTolist();
                return true;
            default:
                return  super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method to clean text input
     */
    private void clean() {
        txtTitle.setText("");
        txtPrice.setText("");
        txtDescription.setText("");
        txtTitle.requestFocus();
    }

    /**
     * Method to save/write the deals to the DB in this instance firebase Db
     */
    private void saveDeal() {
        deal.setTitle( txtTitle.getText().toString());
        deal.setPrice(txtPrice.getText().toString());
        deal.setDescription(txtDescription.getText().toString());

        if(deal.getId() == null)
        {
            // then it is a new deal that we want to insert
            // save to the database (firebase)
            databaseReference.push().setValue(deal);
        }
        else
        {
            // when it is an existing deal, update the deal
            databaseReference.child(deal.getId()).setValue(deal);
        }


    }

    private  void deleteDeal()
    {
        if (deal ==  null)
        {
            Toast.makeText(this, "Please, save thee deal first before deleting.", Toast.LENGTH_SHORT).show();
            return;
        }
        databaseReference.child(deal.getId()).removeValue();

        // delete image related to the deal
        if (deal.getImagName() != null && deal.getImagName().isEmpty() == false){
            StorageReference pictureRef = FireBaseUtils.firebaseStorage.getReference().child(deal.getImagName());
            pictureRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.i("Deal activity", "Deleted successfully");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.i("Deal activity", "Delete fail" + exception.getMessage());
                }
            });
        }
    }

    private void backTolist()
    {
        Intent intent = new Intent(this, ListActivity.class);
        startActivity(intent);
    }



    /**
     * Overridden method to display the menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);

        if (FireBaseUtils.isAdmin == true)
        {
            menu.findItem(R.id.delete_menu).setVisible(true);
            menu.findItem(R.id.save_menu).setVisible(true);
            enableEditText(true);
        }
        else
        {
            menu.findItem(R.id.delete_menu).setVisible(false);
            menu.findItem(R.id.save_menu).setVisible(false);
            enableEditText(false);
        }
        return true;
    }


    private void enableEditText(boolean isEnabled)
    {
        txtPrice.setEnabled(isEnabled);
        txtTitle.setEnabled(isEnabled);
        txtDescription.setEnabled(isEnabled);
    }

    /**
     * Method to display the image from firebase into an imageview
     * @param url
     */
    private void showImage(String url){
        if(url != null && url.isEmpty() == false){
            int width = Resources.getSystem().getDisplayMetrics().widthPixels;
            Picasso.get().load(url).resize(width, width *2/3).centerCrop().into(imageDeal);
        }
    }




}
